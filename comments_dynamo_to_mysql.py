# -*- coding: utf-8 -*-
from logging import getLogger
import sys
import function as fn
from botocore.exceptions import ClientError

logger = getLogger(__name__)
table = fn.get_dynamodb_table(sys.argv, 'ise-km-backend-comments')


def main():
    logger.debug("start")

    try:
        response = table.scan()
        fn.edit_header('article_comments', 'article_comment_reactions', 'article_highlights')
        for item in response['Items']:
            edit_item(item)

        while 'LastEvaluatedKey' in response:
            response = table.scan(
                 ExclusiveStartKey=response['LastEvaluatedKey']
            )

            for item in response['Items']:
                edit_item(item)

    except ClientError as err:
        logger.exception(err)
        raise err
    except Exception as err:
        logger.exception(err)
        raise err

    return "success!"


def edit_item(item):

    # comment
    if 'comments' in item:
        comment_list = list()
        comment_reaction_list = list()
        for ck, cv in item['comments'].items():
            err = fn.edit_user(cv)
            if err:
                print('comment: not found user_serial_id and user_serial_id:{}'.format(ck))
                continue
            fn.add_comment(cv, item['article_id'], item['contract_code'], comment_list)

            # comment reaction
            if 'reactions' in cv:
                for rk, rv in cv['reactions'].items():
                    err = fn.edit_user(rv)
                    if err:
                        print('comment reaction: not found user_serial_id and user_serial_id:{}'.format(rk))
                        continue
                    fn.add_reaction(cv, rv, item['article_id'], item['contract_code'], comment_reaction_list)

            # thread
            if 'threads' in cv:
                for tk, tv in cv['threads'].items():
                    err = fn.edit_user(tv)
                    if err:
                        print('thread: not found user_serial_id and user_serial_id:{}'.format(tk))
                        continue
                    fn.add_thread(cv, tv, item['article_id'], item['contract_code'], comment_list)

                    # thread reaction
                    if 'reactions' in tv:
                        for trk, trv in tv['reactions'].items():
                            err = fn.edit_user(trv)
                            if err:
                                print('comment reaction: not found user_serial_id and user_serial_id:{}'.format(trk))
                                continue
                            fn.add_thread_reaction(tv, rv, item['article_id'], item['contract_code'], comment_reaction_list)
        fn.save_csv("article_comments.csv", comment_list)
        fn.save_csv("article_comment_reactions.csv", comment_reaction_list)

    if 'highlight_comments' in item:
        highlight_list = list()
        for hk, hv in item['highlight_comments'].items():
            err = fn.edit_user(hv)
            if err:
                print('comment: not found user_serial_id and user_serial_id:{}'.format(hk))
                continue
            fn.add_highlight_comment(hv, item['article_id'], item['contract_code'], highlight_list)
        fn.save_csv("article_highlights.csv", highlight_list)


if __name__ == "__main__":
    main()