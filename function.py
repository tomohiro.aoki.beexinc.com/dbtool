# -*- coding: utf-8 -*-
import os
import pandas as pd
import csv
import boto3

BILLION = 1000000000

user_key_pattern = ['user_serial_id', 'user_serial_Id', 'usre_serial_id', 'user_id']

csv_info = [
        {
            'csv': 'article_comments',
            'header': ['comment_id', 'article_id', 'account_code', 'parent_id', 'comment', 'created_at', 'updated_at',
                       'user_serial_id', 'first_name', 'last_name']
        },
        {
            'csv': 'article_comment_reactions',
            'header': ['reaction_id', 'comment_id', 'article_id', 'account_code', 'reaction', 'created_at',
                       'updated_at', 'user_serial_id', 'first_name', 'last_name']
        },
        {
            'csv': 'article_highlights',
            'header': ['highlight_id', 'article_id', 'account_code', 'selection_anchor', 'selection_focus',
                       'start_offset','end_offset', 'created_at', 'updated_at', 'user_serial_id', 'first_name', 'last_name']
        },        {
            'csv': 'workspace_card_comments',
            'header': ['comment_id', 'card_id', 'account_code', 'parent_id', 'comment', 'created_at', 'updated_at',
                       'user_serial_id', 'first_name', 'last_name']
        },
        {
            'csv': 'workspace_card_comment_reactions',
            'header': ['reaction_id', 'comment_id', 'card_id', 'account_code', 'reaction', 'created_at', 'updated_at',
                       'user_serial_id', 'first_name', 'last_name']
        }
    ]


def get_dynamodb_table(argv_param, table_name):
    if len(argv_param) == 2:
        if argv_param[1] == 'prod':
            print('===== prod start =====')
            dynamodb = boto3.resource('dynamodb', region_name='ap-northeast-1')
            return dynamodb.Table(table_name)
        elif argv_param[1] == 'stg':
            print('===== stg start =====')
            table_name = table_name + '-stg'
            dynamodb = boto3.resource('dynamodb', region_name='ap-northeast-1')
            return dynamodb.Table(table_name)
        else:
            print('===== local start =====')
            dynamodb = boto3.resource('dynamodb', endpoint_url='http://localhost:8000')
            return dynamodb.Table(table_name)
    else:
        print('===== no select local start =====')
        dynamodb = boto3.resource('dynamodb', endpoint_url='http://localhost:8000')
        return dynamodb.Table(table_name)


def edit_header(*args):
    for c in csv_info:
        if c['csv'] in args:
            file = c['csv'] + '.csv'
            if os.path.exists(file):
                os.remove(file)
            lst = list()
            lst.append(c['header'])
            df = pd.DataFrame(lst)
            df.to_csv(file, mode='a', header=False, index=False, quotechar='"')


def edit_datetime(object):
    timestamp = int(object['created_at']['Seconds']) * BILLION + int(object['created_at']['Nanos'])
    created_at = pd.to_datetime(timestamp)
    if 'updated_at' in object:
        timestamp = int(object['updated_at']['Seconds']) * BILLION + int(object['updated_at']['Nanos'])
        updated_at = pd.to_datetime(timestamp)
    else:
        updated_at = created_at
    return created_at, updated_at


def edit_user(object):
    user_check = object['created_user'] if 'created_user' in object else object
    for uk in user_key_pattern:
        user_key = uk if uk in user_check else ''
        if user_key != '':
            break
    if user_key == '':
        return True
    if 'created_user' in object:
        object['created_user']['user_serial_id'] = object['created_user'][user_key]
    else:
        object['created_user'] = {'user_serial_id': object[user_key], 'first_name': '', 'last_name': ''}


def save_csv(csv_file, save_list):
    df = pd.DataFrame(save_list)
    df.to_csv(csv_file, mode='a', header=False, index=False, quotechar='"', quoting=csv.QUOTE_NONNUMERIC)


def add_comment(comment, item_id, contract_code, comment_list):
    created_at, updated_at = edit_datetime(comment)
    comment_list.append([comment['comment_id'], item_id, contract_code, '\\N', comment['comment'],
                         created_at, updated_at, comment['created_user']['user_serial_id'],
                         comment['created_user']['first_name'], comment['created_user']['last_name'],
                         ])


def add_thread(comment, thread, item_id, contract_code, comment_list):
    created_at, updated_at = edit_datetime(thread)
    comment_list.append([thread['thread_id'], item_id, contract_code, comment['comment_id'], thread['comment'],
                         created_at, updated_at, thread['created_user']['user_serial_id'],
                         thread['created_user']['first_name'],
                         thread['created_user']['last_name'],
                         ])


def add_highlight_comment(highlight, item_id, contract_code, highlight_list):
    created_at, updated_at = edit_datetime(highlight)
    highlight_list.append([highlight['comment_id'], item_id, contract_code, highlight['selection']['anchor'],
                           highlight['selection']['focust'], highlight['selection']['start_offset'],
                           highlight['selection']['end_offset'], created_at, updated_at,
                           highlight['created_user']['user_serial_id'],
                           highlight['created_user']['first_name'], highlight['created_user']['last_name'],
                           ])


def add_reaction(comment, reaction, item_id, contract_code, reaction_list):
    created_at, updated_at = edit_datetime(reaction)
    reaction_list.append([reaction['reaction_id'], comment['comment_id'], item_id, contract_code,
                          reaction['reaction'], created_at, updated_at, reaction['created_user']['user_serial_id'],
                          reaction['created_user']['first_name'], reaction['created_user']['last_name'],
                          ])


def add_thread_reaction(thread, reaction, item_id, contract_code, reaction_list):
    created_at, updated_at = edit_datetime(reaction)
    reaction_list.append([reaction['reaction_id'], thread['thread_id'], item_id, contract_code,
                          reaction['reaction'], created_at, updated_at, reaction['created_user']['user_serial_id'],
                          reaction['created_user']['first_name'], reaction['created_user']['last_name'],
                          ])